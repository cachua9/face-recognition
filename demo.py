import cv2
import imutils
import torch

from imutils.video import VideoStream
# from mtcnn import MTCNN
from facenet_pytorch import MTCNN
from numpy import expand_dims
from tensorflow.python.keras.models import load_model

import fit_model


def get_embedding(model, face_pixels):
    # scale pixel values
    face_pixels = face_pixels.astype('float32')
    # standardize pixel values across channels (global)
    mean, std = face_pixels.mean(), face_pixels.std()
    face_pixels = (face_pixels - mean) / std
    # transform face into one sample
    samples = expand_dims(face_pixels, axis=0)
    # make prediction to get embedding
    yhat = model.predict(samples)
    return yhat[0]


device = 'cuda' if torch.cuda.is_available() else 'cpu'

# cap = VideoStream(src=0).start()
cap = cv2.VideoCapture(0)

detector = MTCNN(
    margin=50,
    min_face_size=50,
    factor=0.7,
    keep_all=True,
    device=device
)

# detector = MTCNN()

# load the facenet model
model = load_model('facenet_keras.h5')
print('Loaded Model')

model_fit, out_encoder = fit_model.get_model()

count_frame = 0
while True:
    ret, frame = cap.read()
    frame = imutils.resize(frame, width=600)
    frame = cv2.flip(frame, 1)

    count_frame += 1
    if count_frame == 3:
        count_frame = 0

        boxs, p = detector.detect(frame)

        # boxs = []
        # json = detector.detect_faces(frame)
        # for j in json:
        #     boxs.append(j['box'])

        if boxs is not None:
            faces_num = len(boxs)
            print('face found: ', faces_num)

            for box in boxs:
                face = frame[
                       int(box[1]) if int(box[1] > 0) else 0:int(box[3]) if int(box[3] < frame.shape[0]) else
                       frame.shape[0],
                       int(box[0]) if int(box[0] > 0) else 0:int(box[2]) if int(box[2] < frame.shape[1]) else
                       frame.shape[1], :]
                face = cv2.resize(face, (160, 160))

                emb_face = get_embedding(model, face)

                # prediction for the face
                samples = expand_dims(emb_face, axis=0)
                yhat_class = model_fit.predict(samples)
                yhat_prob = model_fit.predict_proba(samples)

                # get name
                class_index = yhat_class[0]
                class_probability = yhat_prob[0, class_index] * 100
                predict_names = out_encoder.inverse_transform(yhat_class)

                # show
                print(box)
                cv2.rectangle(frame, (box[0], box[1]), (box[2], box[3]), (0, 255, 0), 2)
                # cv2.rectangle(frame, (box[0], box[1]), (box[0] + box[2], box[1] + box[3]), (0, 255, 0), 2)
                if class_probability > 95:
                    cv2.putText(frame, '%s (%.2f)' % (predict_names[0], class_probability),
                                (int(box[0]), int(box[1] - 10)), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 255),
                                thickness=1, lineType=1)
                    print('Predicted: %s (%.2f)' % (predict_names[0], class_probability))
                else:
                    cv2.putText(frame, 'unknow', (int(box[0]), int(box[1] - 10)), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1,
                                (0, 0, 255), thickness=1, lineType=1)
                    print('unknow')
        cv2.imshow('Front cam', frame)

    # cv2.imshow('Front cam', frame)

    if cv2.waitKey(1) & 0xFF == ord('x'):
        break

cv2.destroyAllWindows()
