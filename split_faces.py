from facenet_pytorch import MTCNN
import cv2
import os
import torch
import numpy as np

dirPath_face = 'data-full\dataFace'
dirPath_splitted = 'data-full\splitted_data'


def convert(img, detector):
    boxs, p = detector.detect(img)
    if boxs is None:
        return None
    box = boxs[0]
    # max = (box[2] - box[0]) * (box[3] - box[1])
    # for b in boxs:
    #     if (b[2] - b[0]) * (b[3] - b[1]) > max:
    #         max = (b[2] - b[0]) * (b[3] - b[1])
    #         box = b
    x, y, w, h = int(box[0]), int(box[1]), int(box[2] - box[0]), int(box[3] - box[1])
    # x = x - round(w / 5) if (x - round(w / 5) > 0) else 0
    # y = y - round(h / 10) if (y - round(h / 10) > 0) else 0
    # w = w + round(w / 2.5) if (x + w + round(w / 2.5) < img.shape[1]) else img.shape[1]
    # h = h + round(h / 5) if (y + h + round(h / 5) < img.shape[0]) else img.shape[0]

    if w + h < 60:
        return None
    #     print(img.shape)
    #     print(box)
    #     print(x, y, w, h)
    #     pyplot.imshow(img)
    #     ax = pyplot.gca()
    #     rect1 = Rectangle((box[0], box[1]), box[2], box[3], fill=False, color='white')
    #     rect2 = Rectangle((x, y), w, h, fill=False, color='white')
    #     ax.add_patch(rect1)
    #     ax.add_patch(rect2)
    #     pyplot.show()

    return img[y:y + h, x:x + w, :]

def main():
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    detector = MTCNN(
        image_size=60,
        margin=60,
        factor=0.709,
        keep_all=True,
        device=device
    )
    if not os.path.exists(dirPath_face):
        os.makedirs(dirPath_face)
    for fol in os.listdir(dirPath_splitted):
        num_folder = len(np.asarray(os.listdir(os.path.join(dirPath_splitted, fol))))
        crr_folder = 0
        for folder in os.listdir(os.path.join(dirPath_splitted, fol)):
            crr_folder += 1
            if not os.path.exists(os.path.join(dirPath_face, fol, folder)):
                os.makedirs(os.path.join(dirPath_face, fol, folder))
            crr_file = 0
            num_file = len(np.asarray(os.listdir(os.path.join(dirPath_splitted, fol, folder))))
            for file in os.listdir(os.path.join(dirPath_splitted, fol, folder)):
                crr_file += 1
                print(fol, folder, "(%d/%d)" % (crr_folder, num_folder), file, "(%d/%d)" % (crr_file, num_file))
                img = cv2.cvtColor(cv2.imread(os.path.join(dirPath_splitted, fol, folder, file)), cv2.COLOR_BGR2RGB)
                imageFace = convert(img, detector)
                if(imageFace is None):
                    print('--> False')
                    continue
                cv2.imwrite(os.path.join(dirPath_face, fol, folder, file), cv2.cvtColor(imageFace, cv2.COLOR_RGB2BGR))
            #        pyplot.imshow(imageFace)
            #        pyplot.show()

main()