import os
import cv2
import numpy as np

dirPath_raw = 'data-full\dataRaw'
dirPath_test = 'data-full\splitted_data\data_test'
dirPath_train = 'data-full\splitted_data\data_train'


# split data
def get_files_from_folder(path):
    files = os.listdir(path)
    return len(np.asarray(files))


def split_data():
    for folder in os.listdir(dirPath_raw):
        print("In folder: " + folder)
        if not os.path.exists(os.path.join(dirPath_train, folder)):
            os.makedirs(os.path.join(dirPath_train, folder))
        if not os.path.exists(os.path.join(dirPath_test, folder)):
            os.makedirs(os.path.join(dirPath_test, folder))
        num = get_files_from_folder(os.path.join(dirPath_raw, folder))
        num_train = 200
        num_test = num - num_train
        print(num, num_train, num_test)
        index = 0
        for file in os.listdir(os.path.join(dirPath_raw, folder)):
            print(folder, file)
            img = cv2.imread(os.path.join(dirPath_raw, folder, file))
            index += 1
            if index > num_train:
                cv2.imwrite(os.path.join(dirPath_test, folder, file), img)
            else:
                cv2.imwrite(os.path.join(dirPath_train, folder, file), img)
        print("done")


split_data()
