from numpy import load
from numpy import expand_dims
from numpy import asarray
from numpy import savez_compressed
from keras.models import load_model
from mtcnn.mtcnn import MTCNN
import numpy as np
import cv2
import os

def extract_face(filename, required_size=(160, 160)):
    image = cv2.cvtColor(cv2.imread(filename), cv2.COLOR_BGR2RGB)
    detector = MTCNN()
    results = detector.detect_faces(image)
    x1, y1, width, height = results[0]['box']
    x1, y1 = abs(x1), abs(y1)
    x2, y2 = x1 + width, y1 + height
    face = image[y1:y2, x1:x2]
    face = cv2.resize(face, required_size)
    return face
def load_face(filename, required_size=(160, 160)):
    face = cv2.cvtColor(cv2.imread(filename), cv2.COLOR_BGR2RGB)
    face = cv2.resize(face, required_size)
    return face
def load_faces(directory):
    faces = list()
    for filename in os.listdir(directory):
        path = os.path.join(directory, filename)
        face = load_face(path)
        faces.append(face)
    return faces

def load_dataset(directory):
    X, y = list(), list()
    for subdir in os.listdir(directory):
        path = os.path.join(directory, subdir)
        if not os.path.isdir(path):
            continue
        faces = load_faces(path)
        labels = [subdir for _ in range(len(faces))]
        print('>loaded %d examples for class: %s' % (len(faces), subdir))
        X.extend(faces)
        y.extend(labels)
    return asarray(X), asarray(y)

# get the face embedding for one face
def get_embedding(model, face_pixels):
    # scale pixel values
    face_pixels = face_pixels.astype('float32')
    # standardize pixel values across channels (global)
    mean, std = face_pixels.mean(), face_pixels.std()
    face_pixels = (face_pixels - mean) / std
    # transform face into one sample
    samples = expand_dims(face_pixels, axis=0)
    # make prediction to get embedding
    yhat = model.predict(samples)
    return yhat[0]

os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

train_dir = "data-210424\dataFace"

num_classes = len(np.asarray(os.listdir(train_dir)))
print("num of classes:", num_classes)

trainX, trainy = load_dataset(train_dir)
print(trainX.shape, trainy.shape)
savez_compressed('faces-dataset.npz', trainX, trainy)
print('faces-dataset.npz saved')

# load the face dataset
data = load('faces-dataset.npz')
trainX, trainy = data['arr_0'], data['arr_1']
print('Loaded: ', trainX.shape, trainy.shape)

# load the facenet model
model = load_model('facenet_keras.h5')
print('Loaded Model')

# convert each face in the train set to an embedding
newTrainX = list()
for face_pixels in trainX:
    embedding = get_embedding(model, face_pixels)
    newTrainX.append(embedding)
newTrainX = asarray(newTrainX)
print(newTrainX.shape)

# save arrays to one file in compressed format
savez_compressed('faces-embeddings.npz', newTrainX, trainy)
print('faces-embeddings.npz saved')